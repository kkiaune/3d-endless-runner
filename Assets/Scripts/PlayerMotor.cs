﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMotor : MonoBehaviour
{

    private CharacterController controller;
    private Vector3 moveVector;
    private Animator animator;

    private float startTime;
    private float speed = 6.0f;
    private float verticalVelocity = 0.0f;
    private float gravity = 30.0f;
    private float animationDuration = 3.0f;
    private float jumpForce = 12.0f;

    private bool isDead = false;
    private bool shouldJump = false;
    private bool shouldAttack = false;

    public PauseMenu pauseMenu;
    public Joystick joystick;
    public JumpButtonManager jumpButtonManager;
    public AttackButtonManager attackButtonManager;
    public PauseButtonManager pauseButtonManager;

    // Use this for initialization
    void Start()
    {
        controller = GetComponent<CharacterController>();
        animator = GetComponent<Animator>();
        startTime = Time.time;
    }

    // Update is called once per frame
    void Update()
    {
        // do not update if player is dead
        if (isDead)
            return;

        // Do not let player to move while camera effect is on
        if (Time.time - startTime < animationDuration)
        {
            controller.Move(Vector3.forward * speed * Time.deltaTime);
            return;
        }

        moveVector = Vector3.zero;

        // X - left and right
        moveVector.x = (joystick.Horizontal != 0 ? joystick.Horizontal : Input.GetAxisRaw("Horizontal")) * speed;

        // Y - up and down
        if(controller.isGrounded)
        {
            verticalVelocity = -gravity * Time.deltaTime;

            // Jump logic
            if (Input.GetKeyDown(KeyCode.Space) || shouldJump)
            {
                shouldJump = false;
                verticalVelocity = jumpForce;
                animator.SetTrigger("Jump");
                GetComponent<AudioManager>().OnJumpSound();
            }
            // Attack logic
            if (Input.GetKeyDown(KeyCode.Z) || shouldAttack)
            {
                shouldAttack = false;
                animator.SetTrigger("Attack");
                GetComponent<AudioManager>().OnAttackSound();
            }
        }
        else
        {
            verticalVelocity -= (gravity * Time.deltaTime);
        }

        moveVector.y = verticalVelocity;

        // Z - forward and backward 
        moveVector.z = speed;

        controller.Move(moveVector * Time.deltaTime);
    }

    public void SetSpeed(float speedIncrease)
    {
        speed = 6.0f + speedIncrease;
    }

    private void OnControllerColliderHit(ControllerColliderHit hit)
    {
        // traps
        if ( hit.point.z > transform.position.z + 0.1f && (hit.gameObject.tag == "Enemy" || hit.gameObject.tag == "MOB"))
            Death();
        // holes
        if (transform.position.y < -2)
            Death();
    }

    void ToggleUIElements(bool value)
    {
        joystick.ToggleJoystick(value);
        jumpButtonManager.ToggleJumpButton(value);
        attackButtonManager.ToggleAttackButton(value);
        pauseButtonManager.TogglePauseButton(value);
    }

    void Death()
    {
        isDead = true;
        animator.SetTrigger("Death");
        GetComponent<ScoreManager>().OnDeath();
        GetComponent<AudioManager>().OnDeathSound();

        ToggleUIElements(false);
    }

    public void ToggleJump()
    {
        shouldJump = true;
    }

    public void ToggleAttack()
    {
        shouldAttack = true;
    }

    public void Pause()
    {
        Time.timeScale = 0;
        ToggleUIElements(false);
        GetComponent<ScoreManager>().OnPause();
    }

    public void UnPause()
    {
        Time.timeScale = 1;
        ToggleUIElements(true);
        pauseMenu.HideMenu();
    }

    void OnCollisionEnter(Collision col)
    {

        Debug.Log("Hit something");
        if (col.gameObject.name == "Player")
        {
            Debug.Log("Hit the Player");
        }
    }
}
