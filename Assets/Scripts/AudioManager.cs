﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AudioManager : MonoBehaviour {

    // 0 - MainTheme
    // 1 - AttackSound
    // 2 - JumpSound
    // 3 - DeathSound
    public AudioClip[] audioClips;
    public AudioSource[] audioSources;
    public Toggle masterSoundToggle;
    public Toggle secondarySoundToggle;

    private AudioSource primaryAudioSource;
    private AudioSource secondaryAudioSource;
    private bool isSecondarySoundsMuted = false;

    // Use this for initialization
    void Start () {
        primaryAudioSource = audioSources[0];
        if(audioSources.Length > 1)
            secondaryAudioSource = audioSources[1];
        primaryAudioSource.clip = audioClips[0];

        if (PlayerPrefs.GetInt("PrimarySoundPlaying") == 0)
        {
            //TODO cahnge icon here
            primaryAudioSource.Pause();
        }
        else
            primaryAudioSource.Play();

        if(PlayerPrefs.GetInt("SecondarySoundPlaying") == 0)
        {
            //TODO cahnge icon here
            isSecondarySoundsMuted = true;
        }
        else
            isSecondarySoundsMuted = false;
    }
	
	// Update is called once per frame
	void Update () {
    }

    IEnumerator ToggleSecondarySound(AudioClip audioToPlay)
    {
        if (!isSecondarySoundsMuted)
        {
            secondaryAudioSource.clip = audioToPlay;
            secondaryAudioSource.Play();
            yield return new WaitForSeconds(3.0f);
            secondaryAudioSource.Stop();
        }
    }

    public void OnAttackSound()
    {
        StartCoroutine(ToggleSecondarySound(audioClips[1]));
    }

    public void OnJumpSound()
    {
        StartCoroutine(ToggleSecondarySound(audioClips[2]));
    }

    public void OnDeathSound()
    {
        StartCoroutine(ToggleSecondarySound(audioClips[3]));
    }

    public void ToggleMusic()
    {
        if (primaryAudioSource.isPlaying)
        {
            primaryAudioSource.Pause();
            PlayerPrefs.SetInt("PrimarySoundPlaying", 0);
        }
        else{
            primaryAudioSource.UnPause();
            PlayerPrefs.SetInt("PrimarySoundPlaying", 1);
        }
    }

    public void ToggleSound()
    {
        isSecondarySoundsMuted = !isSecondarySoundsMuted;
        PlayerPrefs.SetInt("SecondarySoundPlaying", isSecondarySoundsMuted ? 1 : 0);
    }
}
