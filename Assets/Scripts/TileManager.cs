﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileManager : MonoBehaviour {

    // all possible bridges
    // 0 - w/o any obstacles
    // 1-n - any
    public GameObject[] tilePrefabs;

    private Transform playerTransform;
    private float spawnZ = -5.0f;
    private float tileLength = 10.0f;
    private float safeZone = 30.0f;
    private int amountOfTilesOnScreen = 7;
    private int lastPrefabIndex = 0;

    // current active bridges
    private List<GameObject> activeTiles;

	// Use this for initialization
	void Start () {
        activeTiles = new List<GameObject>();
        playerTransform = GameObject.FindGameObjectWithTag("Player").transform;

        for(int  i = 0; i < amountOfTilesOnScreen; i++)
        {
            // spawn 4 bridges at start for camera effect and bc player can't move
            if (i < 4)
                SpawnTile(0);
            else
                SpawnTile();
        }

    }
	
	// Update is called once per frame
	void Update () {
        if(playerTransform.position.z - safeZone > (spawnZ - amountOfTilesOnScreen * tileLength))
        {
            SpawnTile();
            DeleteTile();
        }

    }

    void SpawnTile(int prefabIndex = -1)
    {
        GameObject go;
        go = Instantiate(tilePrefabs[prefabIndex == -1 ? RandomPrefabIndex() : prefabIndex]) as GameObject;
        go.transform.SetParent(transform);
        go.transform.position = Vector3.forward * spawnZ;
        spawnZ += tileLength;
        activeTiles.Add(go);
    }

    void DeleteTile()
    {
        Destroy(activeTiles[0]);
        activeTiles.RemoveAt(0);
    }

    int RandomPrefabIndex()
    {
        if (tilePrefabs.Length <= 1)
            return 0;

        int randomPrefabIndex = lastPrefabIndex;
        while (randomPrefabIndex == lastPrefabIndex)
            randomPrefabIndex = Random.Range(0, tilePrefabs.Length);

        lastPrefabIndex = randomPrefabIndex;

        return randomPrefabIndex;
    }
}
