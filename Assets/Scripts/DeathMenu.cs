﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class DeathMenu : MonoBehaviour
{

    private bool isOpen = false;
    private float transition = 0.0f;

    public Text scoreText;
    public Image backgroundImg;
    public SoundsMenu soundMenu;

    // Use this for initialization
    void Start()
    {
        gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (!isOpen)
            return;

        // color effect from transparent to black
        transition += Time.deltaTime / 2;
        backgroundImg.color = Color.Lerp(new Color(0, 0, 0, 0), Color.black, transition);
    }

    public void ToggleDeathMenu(float endScore)
    {
        gameObject.SetActive(true);
        soundMenu.ToggleSoundsMenu(true);
        scoreText.text = "Score: " + ((int)endScore).ToString();
        isOpen = true;
    }

    public void Restart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void ToMenu()
    {
        SceneManager.LoadScene("Menu");
    }
}
