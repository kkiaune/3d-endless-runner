﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour {

    private Animator animator;


    // Use this for initialization
    void Start () {
        animator = GetComponent<Animator>();
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void OnDeath()
    {
        animator.SetTrigger("Death");
    }

    public void OnBoxing()
    {
        animator.SetTrigger("Idle");
    }

    void OnCollisionEnter(Collision col)
    {

        Debug.Log("Hit something");
        Debug.Log(col.ToString());
        if (col.gameObject.name == "Player")
        {
            Debug.Log("Hit the Player");
        }
    }
}
