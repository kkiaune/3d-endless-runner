﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpButtonManager : MonoBehaviour {

	// Use this for initialization
	void Start () {
        gameObject.SetActive(true);
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void ToggleJumpButton(bool value = true)
    {
        gameObject.SetActive(value);
    }
}
