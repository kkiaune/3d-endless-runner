﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour
{

    private bool isOpen = false;
    private float transition = 0.5f;

    public Text scoreText;
    public Image backgroundImg;
    public SoundsMenu soundMenu;

    // Use this for initialization
    void Start()
    {
        gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (!isOpen)
            return;

        // color effect from transparent to black
        transition += Time.deltaTime / 2;
        backgroundImg.color = Color.Lerp(new Color(0, 0, 0, 0), Color.black, transition);
    }

    public void TogglePauseMenu(float currentScore)
    {
        gameObject.SetActive(true);
        soundMenu.ToggleSoundsMenu(true);
        scoreText.text = "Current score: " + ((int)currentScore).ToString();
        isOpen = true;
    }

    public void HideMenu()
    {
        soundMenu.ToggleSoundsMenu(false);
        gameObject.SetActive(false);
        isOpen = false;
    }

    public void Restart()
    {
        soundMenu.ToggleSoundsMenu(false);

        // This change game state from idle to play
        Time.timeScale = 1;

        SceneManager.LoadScene("Game");
    }

    public void ToMenu()
    {
        soundMenu.ToggleSoundsMenu(false);

        // This change game state from idle to play
        Time.timeScale = 1;

        SceneManager.LoadScene("Menu");
    }
}
